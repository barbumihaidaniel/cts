package ase.acs.cts.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import ase.acs.cts.auxiliars.AuxiliarFunction;
import ase.acs.cts.exceptions.NumberIsNegativeException;
import ase.acs.cts.exceptions.TooLargeInputException;
import ase.acs.cts.models.Fraction;

public class FractionTest {
	
	public Fraction fraction;

	@Before
	public void setUp() {
		fraction=new Fraction(30,20);
	}
	
	@Test
	public void testExpectedResult() {
		try {
			assertEquals(fraction.calculateHighestCommonDenominator(),10);
		} catch (TooLargeInputException e) {
		} catch (NumberIsNegativeException e) {
		}
	}
	
	@Test
	public void testUpperBoundaries() {
		fraction.setNumarator(Integer.MAX_VALUE);
		try {
			fraction.calculateHighestCommonDenominator();
			fail();
		}catch(TooLargeInputException | NumberIsNegativeException e){
			
			System.out.println(e.getMessage());
		}
		
	}
	
	@Test
	public void testNegativeInput() {
		fraction.setNumarator(-1);
		try {
			fraction.calculateHighestCommonDenominator();
			fail();
		}catch(NumberIsNegativeException | TooLargeInputException e) {
			System.out.println(e.getMessage());
			fraction.setNumarator(20);
		}
	}
	
	@Test
	public void crossCheckTest() {
		
		try {
			assertEquals(fraction.calculateHighestCommonDenominator(), AuxiliarFunction.gcd(fraction.getNumarator(),fraction.getNumitor()));
		} catch (TooLargeInputException e) {
			fail();
			
		} catch (NumberIsNegativeException e) {
			
			fail();
		}
		
	}
	
	
	
}
