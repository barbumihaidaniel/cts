package ase.acs.cts.models;

import ase.acs.cts.exceptions.NumberIsNegativeException;
import ase.acs.cts.exceptions.TooLargeInputException;

public class Fraction {
	private int numitor;
	private int numarator;
	
	public Fraction(){
		
	}
	public Fraction (int numitor,int numarator)
	{
		this.numitor=numitor;
		this.numarator=numarator;
	}
	public int getNumitor() {
		return numitor;
	}
	public void setNumitor(int numitor) {
		this.numitor = numitor;
	}
	public int getNumarator() {
		return numarator;
	}
	public void setNumarator(int numarator) {
		this.numarator = numarator;
	}
	
	public int calculateHighestCommonDenominator() throws TooLargeInputException, NumberIsNegativeException
	{
		int a=this.numarator;
		int b=this.numitor;
		if(a<0||b<0)
		{
			throw new NumberIsNegativeException("One of the numbers is Negative!");
		}
		if(a==Integer.MAX_VALUE||b==Integer.MAX_VALUE) {
			throw new TooLargeInputException("Numbers are too Large!");
		}
		if(a==0) {
			return b;
		}
		
		while(a!=b) {
			if(a>b) {
				a=a-b;
			}
				else{
					b=b-a;
				}
				
		}

		return a;
	}
	
}
