package ase.acs.cts.exceptions;

public class TooLargeInputException extends Exception {
	
	public TooLargeInputException(String message){
		super(message);
	}

}
