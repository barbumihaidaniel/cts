package ase.acs.cts.exceptions;

public class NumberIsNegativeException extends Exception {

		
	public NumberIsNegativeException(String message){
		super(message);
	}
}
