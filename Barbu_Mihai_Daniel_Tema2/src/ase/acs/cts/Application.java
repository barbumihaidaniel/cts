package ase.acs.cts;

import ase.acs.cts.exceptions.NumberIsNegativeException;
import ase.acs.cts.exceptions.TooLargeInputException;
import ase.acs.cts.models.Fraction;

public class Application {

	public static void main(String[] args) {
		Fraction F1= new Fraction(30,50);
		
		try {
			System.out.println(F1.calculateHighestCommonDenominator());
		} catch (TooLargeInputException | NumberIsNegativeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
